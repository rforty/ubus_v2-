package facci.ronny_forty.ubus.Vista.AdminView;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import facci.ronny_forty.ubus.Presentador.AdminPresenter.PresenterAdmin;
import facci.ronny_forty.ubus.R;

public class Horarios_Universidad extends AppCompatActivity implements View.OnClickListener {


    private DatabaseReference mDatabase;
    //  private FirebaseAuth mAuth;
    FloatingActionButton subirHorario;
    EditText et_fecha,et_hora, et_asientos, et_placa;
    PresenterAdmin presenterAdmin;
    private Dialog dialog;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_horarios_universidad);

        subirHorario = findViewById(R.id.btn_subirHorario);
        subirHorario.setOnClickListener(this);
        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
        presenterAdmin = new PresenterAdmin(this, mDatabase); //, mAuth
        initRecycler();


    }

    private void initRecycler() {

        RecyclerView mRecyclerView = findViewById(R.id.recyclerView);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.addItemDecoration(new DividerItemDecoration(this,DividerItemDecoration.VERTICAL));
        presenterAdmin.cargarRecyclerViewUniversidad(mRecyclerView);

    }


    public void cargarBus() {

        dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_row);
        et_fecha = dialog.findViewById(R.id.txt_fecha);
        et_hora = dialog.findViewById(R.id.txt_hora);
      //  et_codigo = dialog.findViewById(R.id.txt_codigo);
        et_asientos = dialog.findViewById(R.id.txt_asientos);
        et_placa = dialog.findViewById(R.id.txt_placa);
        Button subirBus = dialog.findViewById(R.id.btn_agregar);
        subirBus.setOnClickListener(this);
        dialog.show();

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btn_subirHorario:
                cargarBus();
                break;

            case R.id.btn_agregar:

                ComprobarCamposllenos();
                break;
        }
    }


    public void ComprobarCamposllenos() {

        // se guardan en variables String los datos ingresados
        String fecha = et_fecha.getText().toString().trim();
        String hora = et_hora.getText().toString().trim();
       // String codigo = et_codigo.getText().toString().trim();
        String asien = et_asientos.getText().toString().trim();

        String placa = et_placa.getText().toString().trim();

        if (fecha.isEmpty() || hora.isEmpty()  || asien.isEmpty() || placa.isEmpty()) {
            Toast.makeText(this, "Falta llenar datos", Toast.LENGTH_SHORT).show();

        } else {
            // envia los datos al metodo de que carga los datos en la firebase.

            int asientos = Integer.parseInt(asien);
            progressDialog = new ProgressDialog(this);
            progressDialog.setMessage("Subiendo Datos...");
            progressDialog.setCancelable(false);
            progressDialog.show();

            presenterAdmin.cargaBusesFirebaseUniversidad(fecha, hora, asientos, placa, dialog, progressDialog);
        }

    }

}