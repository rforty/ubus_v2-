package facci.ronny_forty.ubus.Vista.LoginView;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import facci.ronny_forty.ubus.Vista.AdminView.Administrador;
import facci.ronny_forty.ubus.Presentador.LoginPresenter.PresentadorLogin;
import facci.ronny_forty.ubus.R;
import facci.ronny_forty.ubus.Vista.MenuView.MainActivity;
import facci.ronny_forty.ubus.Vista.RegisterView.Register;


public class Login extends AppCompatActivity implements View.OnClickListener {
    private EditText mEtxtEmail, mEtxtPassword;
    private PresentadorLogin presentadorLogin;
    FirebaseAuth mAuth;

    @Override
    protected void onStart(){
        super.onStart();

        SharedPreferences tipo_Usuario = getSharedPreferences("emailAdmin", Context.MODE_PRIVATE);

        if (tipo_Usuario.getBoolean("SesionCerrada",true)!=false){

            if(mAuth.getCurrentUser() != null){

                startActivity(new Intent(Login.this, MainActivity.class));
                finish();

            }
        }else{

            Intent intent2 = new Intent (Login.this, Administrador.class);
            startActivity(intent2);

        }





    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

         mAuth = FirebaseAuth.getInstance();
        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
        presentadorLogin = new PresentadorLogin(this, mAuth, mDatabase);
        mEtxtEmail = findViewById(R.id.txt_email);
        mEtxtPassword = findViewById(R.id.txt_psswd);

        mAuth = FirebaseAuth.getInstance();

        SharedPreferences preferences = getSharedPreferences("UserEmail", Context.MODE_PRIVATE);
        mEtxtEmail.setText(preferences.getString("mail", ""));
        mEtxtPassword.setText(preferences.getString("pass",""));


        Button mBtnLogin = findViewById(R.id.btn_login);
        mBtnLogin.setOnClickListener(this);

        Button mBtnRegistrar = findViewById(R.id.button_register);
        mBtnRegistrar.setOnClickListener(this);

        ImageButton mBtnAdmin = findViewById(R.id.button_Admi);
        mBtnAdmin.setOnClickListener(this);

        Button mBtnRecuperar = findViewById(R.id.btn_recu);
        mBtnRecuperar.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btn_login:
                String email = mEtxtEmail.getText().toString().trim();
                String pass = mEtxtPassword.getText().toString().trim();

                SharedPreferences preferencias = getSharedPreferences("UserEmail", Context.MODE_PRIVATE);


                presentadorLogin.ComprobarCamposllenos(preferencias,email, pass);
                break;


            case R.id.button_register:
                Intent intent = new Intent(Login.this, Register.class);
                startActivity(intent);
                break;


            case R.id.button_Admi:
                Intent intent2 = new Intent(Login.this, Administrador.class);
                startActivity(intent2);
                break;


            case R.id.btn_recu :
                Intent intent3 = new Intent (Login.this, ResetPassword.class);
                startActivity(intent3);


        }


    }


}

