package facci.ronny_forty.ubus.Vista.ReservaView;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import facci.ronny_forty.ubus.Presentador.ReservaPresenter.PresenterReserva;
import facci.ronny_forty.ubus.R;
import facci.ronny_forty.ubus.Vista.UserView.destino_casa;

public class Reserva extends AppCompatActivity implements View.OnClickListener {

    TextView txt_hora, txt_asientos,txtfecha;
    DatabaseReference mDatabaseRefe;
    Button mButtonReservar;
    String id_bus;
    FirebaseAuth mAuth;
    PresenterReserva presenterReserva;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reserva);

        txt_hora = findViewById(R.id.tv_hora_casa);
        txt_asientos = findViewById(R.id.tv_asientos_casa);
        txtfecha = findViewById(R.id.fecha_casa);
        mButtonReservar = findViewById(R.id.btn_reservar_Casa);
        mDatabaseRefe = FirebaseDatabase.getInstance().getReference();
        mAuth = FirebaseAuth.getInstance();
        mButtonReservar.setOnClickListener(this);

        mButtonReservar.setEnabled(true);

        Bundle bundle = this.getIntent().getExtras();
        id_bus = bundle.getString("clave");

        presenterReserva = new PresenterReserva(mDatabaseRefe, this, mAuth, mButtonReservar);
        presenterReserva.MostrarDatos(id_bus, "Montecristi", txt_hora, txt_asientos, txtfecha);


    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.btn_reservar_Casa:
                presenterReserva.ConfirmacionReserva("Montecristi", id_bus);


                break;

        }
    }


    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Reserva.this, destino_casa.class);
        startActivity(intent);
        finish();
    }


}

