package facci.ronny_forty.ubus.Vista.AdminView;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import facci.ronny_forty.ubus.Presentador.LoginPresenter.PresentadorLogin;
import facci.ronny_forty.ubus.R;

public class Administrador extends AppCompatActivity implements View.OnClickListener {

    private EditText EmailAdmin, PasswordAdmin;
    private PresentadorLogin presentadorLogin;
    FirebaseAuth mAuth;


    @Override
    protected void onStart() {
        super.onStart();

        SharedPreferences tipo_Usuario = getSharedPreferences("emailAdmin", Context.MODE_PRIVATE);

        if (tipo_Usuario.getBoolean("SesionCerrada", true) == false) {

            if (mAuth.getCurrentUser() != null) {

                startActivity(new Intent(Administrador.this, MenuAdmin.class));
                finish();

            }

        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_administrador);

        mAuth = FirebaseAuth.getInstance();
        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
        presentadorLogin = new PresentadorLogin(this, mAuth, mDatabase);
        EmailAdmin = findViewById(R.id.email_admin);
        PasswordAdmin = findViewById(R.id.psswd_admin);

        SharedPreferences preferences = getSharedPreferences("emailAdmin", Context.MODE_PRIVATE);
        EmailAdmin.setText(preferences.getString("mailAdmin", ""));
        PasswordAdmin.setText(preferences.getString("psswdAdmin",""));


        Button mBtnLogin = findViewById(R.id.btn_login_admin);
        mBtnLogin.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btn_login_admin:
                String email = EmailAdmin.getText().toString().trim();
                String pass = PasswordAdmin.getText().toString().trim();

                SharedPreferences preferencias = getSharedPreferences("emailAdmin", Context.MODE_PRIVATE);
                presentadorLogin.ComprobarCamposllenosAdmin(preferencias, email, pass);

                break;




        }


    }

}
