package facci.ronny_forty.ubus.Vista.ReservaView;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import facci.ronny_forty.ubus.Presentador.ReservaPresenter.PresenterReserva;
import facci.ronny_forty.ubus.R;
import facci.ronny_forty.ubus.Vista.UserView.destino_universidad;

public class Reserva_Uni extends AppCompatActivity implements View.OnClickListener{

    TextView txt_fecha, txt_hora, txt_asientos;
    DatabaseReference mDatabaseRefe;
    Button mButtonReservarUni;
    String id_bus;
    FirebaseAuth mAuth;
    PresenterReserva presenterReserva;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reserva__uni);

        txt_fecha = findViewById(R.id.tv_fecha_uni);
        txt_hora = findViewById(R.id.tv_hora_uni);
        txt_asientos = findViewById(R.id.tv_asientos_uni);
        mButtonReservarUni = findViewById(R.id.btn_reservar_Uni);
        mDatabaseRefe = FirebaseDatabase.getInstance().getReference();
        mAuth = FirebaseAuth.getInstance();
        mButtonReservarUni.setOnClickListener(this);

        Bundle bundle = this.getIntent().getExtras();
        id_bus = bundle.getString("clave");

        presenterReserva = new PresenterReserva(mDatabaseRefe,this,mAuth,mButtonReservarUni);
        presenterReserva.MostrarDatos(id_bus, "Universidad",txt_hora,txt_asientos,txt_fecha);





    }



    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.btn_reservar_Uni:

                presenterReserva.ConfirmacionReserva("Universidad", id_bus);


                break;

        }

    }
    public void onBackPressed() {
        Intent intent = new Intent(Reserva_Uni.this, destino_universidad.class);
        startActivity(intent);
        finish();
    }
}
