package facci.ronny_forty.ubus.Vista.RegisterView;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import facci.ronny_forty.ubus.Presentador.RegisterPresenter.PresentadorRegistro;
import facci.ronny_forty.ubus.R;

public class Register extends AppCompatActivity implements View.OnClickListener{

    private EditText username,email,password,phone,facultad;
    private PresentadorRegistro mPresentadorRegistro;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
        mPresentadorRegistro = new PresentadorRegistro(this,mAuth,mDatabase);
        username  = findViewById(R.id.txt_name_reg);
        email     = findViewById(R.id.txt_email_register);
        password  = findViewById(R.id.txt_pswd_register);
        phone     = findViewById(R.id.txt_phone_reg);
        facultad  = findViewById(R.id.txt_facultad_reg);
       Button buttonRegistrar =findViewById(R.id.btn_registrar_usuario);
       buttonRegistrar.setOnClickListener(this);

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_registrar_usuario:
                String txt_username  = username.getText().toString().trim();
                String txt_email     = email.getText().toString().trim();
                String txt_password  = password.getText().toString().trim();
                String txt_phone     = phone.getText().toString().trim();
                String  txt_facultad  = facultad.getText().toString().trim();

                mPresentadorRegistro.ValidarInformacion(txt_email,txt_password,txt_username,txt_phone,txt_facultad);
                break;
        }

    }
}
