package facci.ronny_forty.ubus.Vista.AdminView;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;

import facci.ronny_forty.ubus.R;

public class MenuAdmin extends AppCompatActivity implements View.OnClickListener {

    Button btn_manta;
    Button btn_montecristi;
    Button btn_salir;
    Button btn_lista;
    private FirebaseAuth mAuth;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_admin);

        Toast.makeText(this,"Bienvenido Administrador", Toast.LENGTH_SHORT).show();

        mAuth = FirebaseAuth.getInstance();
        btn_manta = findViewById(R.id.btn_casa_admin);
        btn_manta.setOnClickListener(this);

        btn_salir = findViewById(R.id.btn_cerrar_sesion_admin);
        btn_salir.setOnClickListener(this);

        btn_montecristi = findViewById(R.id.btn_universidad_admin);
        btn_montecristi.setOnClickListener(this);


        btn_lista = findViewById(R.id.btn_lista_User);
        btn_lista.setOnClickListener(this);




    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){

            case R.id.btn_casa_admin : Intent intent = new Intent(MenuAdmin.this, Horarios_Casa.class);
                startActivity(intent);

                break;

            case R.id.btn_universidad_admin : Intent intent2 = new Intent(MenuAdmin.this, Horarios_Universidad.class);
                startActivity(intent2);
                break;

            case R.id.btn_lista_User : Intent intent3 = new Intent (MenuAdmin.this, Lista_Usuarios.class);
                startActivity(intent3);
                break;

            case R.id.btn_cerrar_sesion_admin :

                CerrarSesion(mAuth,this);


                break;

        }

    }


    public void CerrarSesion(final FirebaseAuth firebaseAuth, final Context mContext) {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:


                        SharedPreferences tipo_Usuario = getSharedPreferences("emailAdmin", Context.MODE_PRIVATE);
                        SharedPreferences.Editor Obj_editor = tipo_Usuario.edit();
                        Obj_editor.putBoolean("SesionCerrada", true);
                        Obj_editor.commit();

                        firebaseAuth.signOut();

                        Intent intent3 = new Intent (mContext, Administrador.class);
                        mContext.startActivity(intent3);
                        ((Activity) mContext).finish();

                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle("¿Esta que seguro que desea cerrar sesión?")

                .setPositiveButton("Cerrar Sesión", dialogClickListener)
                .setNegativeButton("No", dialogClickListener)
                .show();

    }

    @Override
    public void onBackPressed() {
        CerrarSesion(mAuth,this);
    }


}
