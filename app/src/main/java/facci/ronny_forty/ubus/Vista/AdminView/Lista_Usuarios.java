package facci.ronny_forty.ubus.Vista.AdminView;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import facci.ronny_forty.ubus.Presentador.AdminPresenter.PresenterAdmin;
import facci.ronny_forty.ubus.R;

public class Lista_Usuarios extends AppCompatActivity {

    PresenterAdmin presenterListaUser;
    private DatabaseReference mDatabase;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista__usuarios);
        mDatabase = FirebaseDatabase.getInstance().getReference();
        presenterListaUser = new PresenterAdmin(this, mDatabase);
        initRecycler();
    }


    private void initRecycler() {

        RecyclerView mRecyclerView = findViewById(R.id.recyclerViewListaUser); // conexion con el componente recycler view
        LinearLayoutManager layoutManager = new LinearLayoutManager(this); //
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.addItemDecoration(new DividerItemDecoration(this,DividerItemDecoration.VERTICAL));
        presenterListaUser.cargarListaUsuario(mRecyclerView); // objeto de clase Presentador que ejecuta metodo
        //donde pasamos como parametro el recycler

    }
}
