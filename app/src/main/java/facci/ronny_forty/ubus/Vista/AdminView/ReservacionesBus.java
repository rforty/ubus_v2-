package facci.ronny_forty.ubus.Vista.AdminView;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import facci.ronny_forty.ubus.Presentador.AdminPresenter.PresenterAdmin;
import facci.ronny_forty.ubus.R;

public class ReservacionesBus extends AppCompatActivity {

    PresenterAdmin presenterReservaciones;
    private DatabaseReference mDatabase;
    String bus_key;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reservaciones_bus);
        mDatabase = FirebaseDatabase.getInstance().getReference();
        presenterReservaciones = new PresenterAdmin(this, mDatabase);
        Bundle bundleKey = this.getIntent().getExtras();
        bus_key = bundleKey.getString("key_bus");
        initRecycler();
    }

    private void initRecycler() {

        RecyclerView mRecyclerView = findViewById(R.id.recyclerViewReservacionesBus); // conexion con el componente recycler view
        LinearLayoutManager layoutManager = new LinearLayoutManager(this); //
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.addItemDecoration(new DividerItemDecoration(this,DividerItemDecoration.VERTICAL));
        presenterReservaciones.cargarReservaciones(mRecyclerView,"Montecristi","Universidad",bus_key); // objeto de clase Presentador que ejecuta metodo
        //donde pasamos como parametro el recycler

    }
}
