package facci.ronny_forty.ubus.Vista.ReservadoView;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import facci.ronny_forty.ubus.Presentador.ReservadoPresenter.PresenterReservado;
import facci.ronny_forty.ubus.R;
import facci.ronny_forty.ubus.Vista.MenuView.MainActivity;


public class Asiento_Reservado extends AppCompatActivity implements View.OnClickListener {

    String id_reserva;
    ImageButton btn_returnMain;
    TextView txt_fecha, txt_user, txt_hora, txt_placa, txt_destino, txt_asiento;
    Button btn_borrar_reg; // btn_cancelar
    FirebaseAuth mAuth;
    DatabaseReference mDatabaseRefe;
    PresenterReservado presenterReservado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_asiento__reservado);
        txt_fecha = findViewById(R.id.tv_fecha_m);
        txt_user = findViewById(R.id.tv_user_reser);
        txt_hora = findViewById(R.id.tv_hora);
        txt_placa = findViewById(R.id.tv_placa);
        //txt_codigo = findViewById(R.id.tv_cod);
        txt_asiento = findViewById(R.id.tv_asiento);
        txt_destino = findViewById(R.id.tv_destino);
        btn_returnMain = findViewById(R.id.ib_returnMain);

       // btn_cancelar = findViewById(R.id.btn_cancelar_reserva);
        //btn_cancelar.setOnClickListener(this);

        btn_borrar_reg = findViewById(R.id.btn_Borrar_registro);
        btn_borrar_reg.setOnClickListener(this);

        btn_returnMain = findViewById(R.id.ib_returnMain);
        btn_returnMain.setOnClickListener(this);

        mAuth = FirebaseAuth.getInstance();
        mDatabaseRefe = FirebaseDatabase.getInstance().getReference();

        Bundle bundleAsiento = this.getIntent().getExtras();
        id_reserva = bundleAsiento.getString("key");

        presenterReservado = new PresenterReservado(mAuth, mDatabaseRefe, Asiento_Reservado.this, id_reserva);

        presenterReservado.SendNameUser(txt_user);

        presenterReservado.MostrarDatos(txt_hora, txt_placa, txt_destino,txt_fecha,txt_asiento);




    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.btn_Borrar_registro:
                presenterReservado.EliminacionRegistro();



                break;

/*
            case R.id.btn_cancelar_reserva:
                presenterReservado.CancelarReservacion();
                break;

                */

            case R.id.ib_returnMain:
                Intent intent = new Intent(Asiento_Reservado.this, MainActivity.class);
                startActivity(intent);
                finish();


        }


    }
}






