package facci.ronny_forty.ubus.Vista.ReservaView;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import facci.ronny_forty.ubus.Presentador.ReservaPresenter.PresenterReservaciones;
import facci.ronny_forty.ubus.R;

public class Reservaciones extends AppCompatActivity {

    PresenterReservaciones presenterReservar;
    private DatabaseReference mDatabase;
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reservaciones);
         mDatabase = FirebaseDatabase.getInstance().getReference();
         mAuth = FirebaseAuth.getInstance();


        presenterReservar = new PresenterReservaciones(this, mDatabase, mAuth);

        initRecycler(); // cuando se abre la activity se ejecuta el metodo que muestra todos los items
        //del recyclerView

    }

    private void initRecycler() {

        RecyclerView mRecyclerView = findViewById(R.id.recyclerViewReservaciones); // conexion con el componente recycler view
        LinearLayoutManager layoutManager = new LinearLayoutManager(this); //
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.addItemDecoration(new DividerItemDecoration(this,DividerItemDecoration.VERTICAL));
        presenterReservar.cargarRecyclerViewReservaciones(mRecyclerView); // objeto de clase Presentador que ejecuta metodo
        //donde pasamos como parametro el recycler

    }


}
