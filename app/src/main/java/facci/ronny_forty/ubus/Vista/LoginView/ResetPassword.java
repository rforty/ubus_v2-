package facci.ronny_forty.ubus.Vista.LoginView;

import android.app.ProgressDialog;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

import facci.ronny_forty.ubus.R;

public class ResetPassword extends AppCompatActivity {

    EditText txtemail;
    Button btn_recuperar;

    private String email;

    private  FirebaseAuth mAuth;
    ProgressDialog mDialog ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);

        mAuth = FirebaseAuth.getInstance();

        txtemail = findViewById(R.id.et_reset);
        btn_recuperar = findViewById(R.id.btn_reset_psswd);
        mDialog= new ProgressDialog(this);

        btn_recuperar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                email = txtemail.getText().toString();

                if(!email.isEmpty()){
                   mDialog.setMessage("Enviando correo...");
                   mDialog.setCanceledOnTouchOutside(false);
                    mDialog.show();
                    RecuperarContraseña();



                }
                else {
                    Toast.makeText(ResetPassword.this, "Debe ingresar el email", Toast.LENGTH_LONG).show();
                }

            }
        });
    }

    public  void RecuperarContraseña (){
        mAuth.setLanguageCode("es");
        mAuth.sendPasswordResetEmail(email).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {

                if (task.isSuccessful()){
                    Toast.makeText(ResetPassword.this, "Se ha enviado un correo para restablecer su contraseña",
                            Toast.LENGTH_LONG).show();

                } else {
                    Toast.makeText(ResetPassword.this, "No se pudo enviar el correo de restablecer contraseña",
                            Toast.LENGTH_LONG).show();

                }
                mDialog.dismiss();
            }
        });


    }


}
