

package facci.ronny_forty.ubus.Vista.MenuView;


import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import facci.ronny_forty.ubus.Presentador.MainPresenter.PresentadorMain;
import facci.ronny_forty.ubus.R;
import facci.ronny_forty.ubus.Vista.LoginView.Login;
import facci.ronny_forty.ubus.Vista.ReservaView.Reservaciones;
import facci.ronny_forty.ubus.Vista.UserView.destino_casa;
import facci.ronny_forty.ubus.Vista.UserView.destino_universidad;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    Button button_home, button_university, button_salir;
    Button button_reservaciones;
  //  TextView tv_user1;
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;
    PresentadorMain presentadorMain;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //tv_user1 =findViewById(R.id.tv_user);
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        button_home = findViewById(R.id.button_home);
        button_home.setOnClickListener(this);

        button_university = findViewById(R.id.button_university);
        button_university.setOnClickListener(this);


        presentadorMain = new PresentadorMain();
        presentadorMain.SaludarUsuario(mDatabase,mAuth,MainActivity.this);

        button_salir = findViewById(R.id.btn_cerrar_sesion);
        button_salir.setOnClickListener(this);


        button_reservaciones = findViewById(R.id.btn_listaReservaciones);
        button_reservaciones.setOnClickListener(this);




    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){

            case R.id.button_home : Intent intent = new Intent(MainActivity.this,destino_casa.class);
            startActivity(intent);
            break;

            case R.id.button_university : Intent intent2 = new Intent(MainActivity.this, destino_universidad.class);
            startActivity(intent2);
            break;


            case R.id.btn_cerrar_sesion :
                CerrarSesion();

            break;


            case R.id.btn_listaReservaciones : Intent intent4 = new Intent(MainActivity.this, Reservaciones.class);
            startActivity(intent4);
            break;

        }

    }


    private void CerrarSesion() {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:

                        mAuth.signOut();
                        Intent intent3 = new Intent (MainActivity.this, Login.class);
                        startActivity(intent3);
                        finish();
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("¿Esta que seguro que desea  cerrar sesión?")

                .setPositiveButton("Cerrar Sesión", dialogClickListener)
                .setNegativeButton("No", dialogClickListener)
                .show();

    }

    @Override
    public void onBackPressed() {
        CerrarSesion();
    }
}
