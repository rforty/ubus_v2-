package facci.ronny_forty.ubus.Vista.UserView;


import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import facci.ronny_forty.ubus.Presentador.UserPresenter.PresenterUserUni;
import facci.ronny_forty.ubus.R;

public class destino_universidad extends AppCompatActivity {

    private DatabaseReference mDatabase;
    private FirebaseAuth mAuth;
    PresenterUserUni presenterUserUni;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_destino_universidad);
        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
        presenterUserUni = new PresenterUserUni(this,mDatabase,mAuth);
        initRecycler();






    }

    private  void  initRecycler(){

        RecyclerView mRecyclerView = findViewById(R.id.recyclerViewUserUni);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);
        //mRecyclerView.addItemDecoration(new DividerItemDecoration(this,DividerItemDecoration.VERTICAL));
        presenterUserUni.cargarRecyclerViewUserUniversidad(mRecyclerView);


    }
}
