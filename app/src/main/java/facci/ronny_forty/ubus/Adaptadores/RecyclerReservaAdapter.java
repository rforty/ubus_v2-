package facci.ronny_forty.ubus.Adaptadores;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import facci.ronny_forty.ubus.Modelo.ReservacionModel;
import facci.ronny_forty.ubus.R;
import facci.ronny_forty.ubus.Vista.ReservadoView.Asiento_Reservado;
//import facci.ronny_forty.ubus.Vista.ReservadoView.Asiento_Reservado;

//5
//extends RecyclerView.Adapter<RecyclerBusAdapter.BusViewHolder> e implementamos los 3 metodos
public class RecyclerReservaAdapter extends RecyclerView.Adapter<RecyclerReservaAdapter.ReservaViewHolder> {

    //1
    private Context mContext;
    private int layoutResource; //es el resource que va a llenar cada elemento de la vista de cada item del recycler
    private ArrayList<ReservacionModel> arrayListReserva;


    //2 Constructor
    public RecyclerReservaAdapter(Context mContext, int layoutResource, ArrayList<ReservacionModel> arrayListReserva) {
        this.mContext = mContext;
        this.layoutResource = layoutResource;
        this.arrayListReserva = arrayListReserva;
    }



    //metodo 2
    @NonNull
    @Override
    public ReservaViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        // infla la vista que viene de este layoutResource
        View view = LayoutInflater.from(mContext).inflate(layoutResource, viewGroup, false);
        return new ReservaViewHolder(view);
    }

    //metodo 3
    @Override
    // el parametro BusViewHolder retorna una vista
    public void onBindViewHolder(@NonNull ReservaViewHolder reservaViewHolder, int position) {
        //obtenemos posicion del array
        //enviamos datos a los componentes

        ReservacionModel reservaModel = arrayListReserva.get(position);

        reservaViewHolder.mFecha.setText(reservaModel.getFecha());
        reservaViewHolder.mDestino.setText(reservaModel.getDestino());
        reservaViewHolder.mHora.setText(reservaModel.getHora());
        reservaViewHolder.mAsiento.setText(String.valueOf(reservaModel.getAsiento()));
        reservaViewHolder.mKey.setText(reservaModel.getClave());
        reservaViewHolder.mImageRese.setImageResource(R.drawable.ic_directions_bus_blue_80dp);



    }


    // metodo 1
    @Override
    public int getItemCount() {

        if (arrayListReserva.size() > 0) {
            return arrayListReserva.size();
        }
        return 0;
    }

    //3    Creamos ViewHolder con su Constructor, despues se crea el diseño del BusRow
    public class ReservaViewHolder extends RecyclerView.ViewHolder implements  View.OnClickListener {

        //Creamos objetos para conectar con bus_Row
        TextView  mFecha,mDestino, mAsiento,mHora, mKey;
        ImageView mImageRese;

        public ReservaViewHolder(@NonNull View itemView) {

            //4
            //se realizan las conexiones con los coponentes del reserva_row
            //utilizamos itemView por que estamos utilzando la vista del row para llenar el recycler
            super(itemView);

            mFecha = itemView.findViewById(R.id.row_fecha);
            mImageRese = itemView.findViewById(R.id.im_bus_reservado);
            mDestino = itemView.findViewById(R.id.row_destino);
            mHora = itemView.findViewById(R.id.row_hora);
            mAsiento = itemView.findViewById(R.id.row_asiento);
            mKey = itemView.findViewById(R.id.row_key);
            itemView.setOnClickListener(this);


        }

        @Override
        public void onClick(View v) {


            String clave = mKey.getText().toString().trim();
            Intent intent = new Intent(mContext, Asiento_Reservado.class);
            Bundle bundleAsiento = new Bundle();
            bundleAsiento.putString("key", clave);
            intent.putExtras(bundleAsiento);
            mContext.startActivity(intent);





        }
    }



}

