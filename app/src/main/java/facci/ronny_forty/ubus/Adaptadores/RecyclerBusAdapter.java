package facci.ronny_forty.ubus.Adaptadores;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import facci.ronny_forty.ubus.Modelo.BusModel;
import facci.ronny_forty.ubus.Presentador.AdminPresenter.PresenterAdmin;
import facci.ronny_forty.ubus.R;
import facci.ronny_forty.ubus.Vista.AdminView.ReservacionesBus;


//5
//extends RecyclerView.Adapter<RecyclerBusAdapter.BusViewHolder> e implementamos los 3 metodos
public class RecyclerBusAdapter extends RecyclerView.Adapter<RecyclerBusAdapter.BusViewHolder> {

    //1
    private Context mContext;
    private int layoutResource; //es el resource que va a llenar cada elemento de la vista de cada item del recycler
    private ArrayList<BusModel> arrayListBuses;


    //2 Constructor
    public RecyclerBusAdapter(Context mContext, int layoutResource, ArrayList<BusModel> arrayListBuses) {
        this.mContext = mContext;
        this.layoutResource = layoutResource;
        this.arrayListBuses = arrayListBuses;
    }



    //metodo 2
    @NonNull
    @Override
    public BusViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        // infla la vista que viene de este layoutResource
        View view = LayoutInflater.from(mContext).inflate(layoutResource, viewGroup, false);
        return new BusViewHolder(view);
    }

    //metodo 3
    @Override
    // el parametro BusViewHolder retorna una vista
    public void onBindViewHolder(@NonNull BusViewHolder busViewHolder, int position) {
        //obtenemos posicion del array
        //enviamos datos a los componentes
        BusModel busModel = arrayListBuses.get(position);
        busViewHolder.mFechaBus.setText(busModel.getFecha());
        busViewHolder.mHorabus.setText(busModel.getHora());
       // busViewHolder.mCodigobus.setText(busModel.getCodigo());
        busViewHolder.mAsientosbus.setText(String.valueOf(busModel.getAsientos()));
        busViewHolder.mPlacabus.setText(busModel.getPlaca());
        busViewHolder.mImagebus.setImageResource(R.drawable.ic_directions_bus_white_24dp);
        busViewHolder.bus_id.setText(busModel.getId_bus());


    }


    // metodo 1
    @Override
    public int getItemCount() {

        if (arrayListBuses.size() > 0) {
            return arrayListBuses.size();
        }
        return 0;
    }

    //3    Creamos ViewHolder con su Constructor, despues se crea el diseño del BusRow
    public class BusViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        //Creamos objetos para conectar con bus_Row
        TextView mHorabus, mAsientosbus, mPlacabus,mFechaBus;
        ImageView mImagebus;
        TextView bus_id;
        Button mBtn_eliminar;


        public BusViewHolder(@NonNull  View itemView) {

            //4
            //se realizan las conexiones con los coponentes del bus_row
            //utilizamos itemView por que estamos utilzando la vista del row para llenar el recycler
            super(itemView);
            mFechaBus = itemView.findViewById(R.id.tv_rowfecha);
            mHorabus = itemView.findViewById(R.id.tv_rowhora);
          //  mCodigobus = itemView.findViewById(R.id.tv_rowcodigo);
            mAsientosbus = itemView.findViewById(R.id.tv_rowasientos);
            mPlacabus = itemView.findViewById(R.id.tv_rowplaca);
            mImagebus = itemView.findViewById(R.id.iv_rowbus);
            bus_id = itemView.findViewById(R.id.keyAdmin);
            itemView.setOnClickListener(this);

            mBtn_eliminar = itemView.findViewById(R.id.btn_Eliminar);
            mBtn_eliminar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    String clave = bus_id.getText().toString().trim();
                    ConfirmacionEliminacion(clave);

                }
            });


        }

        @Override
        public void onClick(View v) {
            String key_bus = bus_id.getText().toString().trim();
            Intent intent  = new Intent(mContext, ReservacionesBus.class);
            Bundle bundleKey = new Bundle();
            bundleKey.putString("key_bus", key_bus);
            intent.putExtras(bundleKey);
            mContext.startActivity(intent);


        }
    }

    private void ConfirmacionEliminacion(final String key) {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:


                        PresenterAdmin presenter = new PresenterAdmin();

                        presenter.Eliminar(key);

                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle("Eliminar Viaje")
                .setMessage("Se recomienda eliminar después de la hora de partida")
                .setPositiveButton("  Si", dialogClickListener)
                .setNegativeButton("  No", dialogClickListener)
                .show();

    }



}
