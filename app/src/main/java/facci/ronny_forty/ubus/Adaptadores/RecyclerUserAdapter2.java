package facci.ronny_forty.ubus.Adaptadores;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import facci.ronny_forty.ubus.Modelo.UserModel;
import facci.ronny_forty.ubus.R;


//5
//extends RecyclerView.Adapter<RecyclerBusAdapter.BusViewHolder> e implementamos los 3 metodos
public class RecyclerUserAdapter2 extends RecyclerView.Adapter<RecyclerUserAdapter2.UserViewHolder> {

    //1
    private Context mContext;
    private int layoutResource; //es el resource que va a llenar cada elemento de la vista de cada item del recycler
    private ArrayList<UserModel> arrayListUser;


    //2 Constructor
    public RecyclerUserAdapter2(Context mContext, int layoutResource, ArrayList<UserModel> arrayListUser) {
        this.mContext = mContext;
        this.layoutResource = layoutResource;
        this.arrayListUser = arrayListUser;
    }



    //metodo 2
    @NonNull
    @Override
    public UserViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        // infla la vista que viene de este layoutResource
        View view = LayoutInflater.from(mContext).inflate(layoutResource, viewGroup, false);
        return new UserViewHolder(view);
    }

    //metodo 3
    @Override
    // el parametro BusViewHolder retorna una vista
    public void onBindViewHolder(@NonNull UserViewHolder userViewHolder, int position) {
        //obtenemos posicion del array
        //enviamos datos a los componentes
        UserModel userModel = arrayListUser.get(position);
        userViewHolder.mNombre.setText(userModel.getNombre());
        userViewHolder.mTelefono.setText(userModel.getTelefono());
        userViewHolder.mFacultad.setText(userModel.getFacultad());




    }


    // metodo 1
    @Override
    public int getItemCount() {

        if (arrayListUser.size() > 0) {
            return arrayListUser.size();
        }
        return 0;
    }

    //3    Creamos ViewHolder con su Constructor, despues se crea el diseño del BusRow
    public class UserViewHolder extends RecyclerView.ViewHolder {

        //Creamos objetos para conectar con bus_Row
        TextView mNombre, mTelefono, mFacultad;



        public UserViewHolder(@NonNull View itemView) {

            //4
            //se realizan las conexiones con los coponentes del bus_row
            //utilizamos itemView por que estamos utilzando la vista del row para llenar el recycler
            super(itemView);
            mNombre = itemView.findViewById(R.id.row_nombre_2);
            mTelefono = itemView.findViewById(R.id.row_telefono_2);
            mFacultad = itemView.findViewById(R.id.row_facultad_2);




        }

    }




}

