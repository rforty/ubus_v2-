package facci.ronny_forty.ubus.Adaptadores;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import facci.ronny_forty.ubus.Modelo.BusModel;
import facci.ronny_forty.ubus.R;
import facci.ronny_forty.ubus.Vista.ReservaView.Reserva_Uni;

public class RecyclerBusAdapterUserUni extends RecyclerView.Adapter<RecyclerBusAdapterUserUni.BusViewHolderUserUni> {


    private Context mContext;
    private int layoutResource;
    private ArrayList<BusModel> arrayListBusesUserUni;


    BusModel busModel;

    public RecyclerBusAdapterUserUni(Context mContext, int layoutResource, ArrayList<BusModel> arrayListBuses) {
        this.mContext = mContext;
        this.layoutResource = layoutResource;
        this.arrayListBusesUserUni = arrayListBuses;
    }

    @NonNull
    @Override
    public BusViewHolderUserUni onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(mContext).inflate(layoutResource, viewGroup, false);
        return new BusViewHolderUserUni(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BusViewHolderUserUni busViewHolderUserUni, int position) {
        busModel = arrayListBusesUserUni.get(position);

        busViewHolderUserUni.mFechabus.setText(busModel.getFecha());
        busViewHolderUserUni.mHorabus.setText(busModel.getHora());
        busViewHolderUserUni.mAsientosbus.setText(String.valueOf(busModel.getAsientos()));
        busViewHolderUserUni.mImagebus.setImageResource(R.drawable.ic_directions_bus_white_24dp);
        busViewHolderUserUni.id_bus.setText(busModel.getId_bus());

    }


    @Override
    public int getItemCount() {

        if (arrayListBusesUserUni.size() > 0) {
            return arrayListBusesUserUni.size();
        }
        return 0;
    }


    public class BusViewHolderUserUni extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView mFechabus, mHorabus, mAsientosbus;
        ImageView mImagebus;
        TextView id_bus;

        public BusViewHolderUserUni(@NonNull View itemView) {
            super(itemView);

            itemView.setOnClickListener(this);
            //mButtonReservar.setOnClickListener(this);
            mFechabus = itemView.findViewById(R.id.tv_rowfecha_user_uni);
            mHorabus = itemView.findViewById(R.id.tv_rowhora_user_uni);
            mAsientosbus = itemView.findViewById(R.id.tv_rowasientos_user_uni);
            mImagebus = itemView.findViewById(R.id.iv_rowbus_user_uni);
            id_bus = itemView.findViewById(R.id.key2_uni);


        }


        @Override
        public void onClick(View v) {
            String clave = id_bus.getText().toString().trim();
            Intent intent = new Intent(mContext, Reserva_Uni.class);

            Bundle bundle = new Bundle();
            bundle.putString("clave", clave);
            intent.putExtras(bundle);
            mContext.startActivity(intent);
            ((Activity)mContext).finish();


        }


    }


}