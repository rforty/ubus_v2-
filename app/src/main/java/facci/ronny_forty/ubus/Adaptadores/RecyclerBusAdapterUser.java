package facci.ronny_forty.ubus.Adaptadores;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;

import java.util.ArrayList;

import facci.ronny_forty.ubus.Modelo.BusModel;
import facci.ronny_forty.ubus.R;
import facci.ronny_forty.ubus.Vista.ReservaView.Reserva;

public class RecyclerBusAdapterUser extends RecyclerView.Adapter<RecyclerBusAdapterUser.BusViewHolderUser> {

    private DatabaseReference mDatabase;
    private Context mContext;
    private int layoutResource;
    private ArrayList<BusModel> arrayListBusesUser;


    BusModel busModel;

    public RecyclerBusAdapterUser(Context mContext, int layoutResource, ArrayList<BusModel> arrayListBuses) {
        this.mContext = mContext;
        this.layoutResource = layoutResource;
        this.arrayListBusesUser = arrayListBuses;
    }

    @NonNull
    @Override
    public BusViewHolderUser onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(mContext).inflate(layoutResource, viewGroup, false);
        return new BusViewHolderUser(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BusViewHolderUser busViewHolderUser, int position) {
        busModel = arrayListBusesUser.get(position);

        busViewHolderUser.mFechabus.setText(busModel.getFecha());
        busViewHolderUser.mHorabus.setText(busModel.getHora());
        busViewHolderUser.mAsientosbus.setText(String.valueOf(busModel.getAsientos()));
        busViewHolderUser.mImagebus.setImageResource(R.drawable.ic_directions_bus_white_24dp);
        busViewHolderUser.id_bus.setText(busModel.getId_bus());


    }


    @Override
    public int getItemCount() {

        if (arrayListBusesUser.size() > 0) {
            return arrayListBusesUser.size();
        }
        return 0;
    }


    public class BusViewHolderUser extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView mFechabus,mHorabus, mAsientosbus;
        ImageView mImagebus;
        TextView id_bus;


        public BusViewHolderUser(@NonNull View itemView) {
            super(itemView);

            itemView.setOnClickListener(this);
            mHorabus = itemView.findViewById(R.id.tv_rowhora_user);
            mAsientosbus = itemView.findViewById(R.id.tv_rowasientos_user);
            mImagebus = itemView.findViewById(R.id.iv_rowbus_user);
            mFechabus = itemView.findViewById(R.id.tv_rowfecha_user);
            id_bus = itemView.findViewById(R.id.key2);
        }


        @Override
        public void onClick(View v) {
            String clave = id_bus.getText().toString().trim();
            Intent intent = new Intent(mContext, Reserva.class);
            Bundle bundle = new Bundle();
            bundle.putString("clave", clave);
            intent.putExtras(bundle);
            mContext.startActivity(intent);
            ((Activity)mContext).finish();


        }


    }


}