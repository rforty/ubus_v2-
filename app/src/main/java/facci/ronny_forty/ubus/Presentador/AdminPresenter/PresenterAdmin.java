package facci.ronny_forty.ubus.Presentador.AdminPresenter;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import facci.ronny_forty.ubus.Adaptadores.RecyclerBusAdapter;
import facci.ronny_forty.ubus.Adaptadores.RecyclerUserAdapter;
import facci.ronny_forty.ubus.Adaptadores.RecyclerUserAdapter2;
import facci.ronny_forty.ubus.Modelo.BusModel;
import facci.ronny_forty.ubus.Modelo.UserModel;
import facci.ronny_forty.ubus.R;

public class PresenterAdmin {

    private Context mCOntext;
    private DatabaseReference mDatabase;


    //6
    private RecyclerBusAdapter mAdapter;
    private RecyclerUserAdapter mAdapterUser;
    private  RecyclerUserAdapter2 mAdapterUser2;

    public PresenterAdmin() {
    }

    public PresenterAdmin(Context mCOntext, DatabaseReference mDatabase) { // FirebaseAuth mAuth
        this.mCOntext = mCOntext;
        this.mDatabase = mDatabase;
        //this.mAuth = mAuth;
    }

    //7
    //Metodo para obtener los datos de Firebase
    public void cargarRecyclerView(final RecyclerView mRecyclerView) {


        // para cargar los items se obtiene los datos de la firebase
        mDatabase.child("Buses").child("Montecristi").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                ArrayList<BusModel> arrayListBuses = new ArrayList<>(); // creamoa ArrayList

                for (DataSnapshot snapshot : dataSnapshot.getChildren()) { //capturamos de los nodos hijos (es decir todas esa claves)
                    //el nombre de los datos de firebase deben ser los mismo que los de la clase BusModel
                    BusModel busModel = snapshot.getValue(BusModel.class);
                    busModel.setFecha((busModel.getFecha()));
                    busModel.setHora((busModel.getHora()));
                   // busModel.setCodigo((busModel.getCodigo()));
                    int num_asientos = busModel.getAsientos();
                    busModel.setAsientos(num_asientos);
                    busModel.setPlaca((busModel.getPlaca()));
                    busModel.setId_bus(busModel.getId_bus());

                    arrayListBuses.add(busModel);

                }


                //utilizamos la clase RecyclerBusAdapter
                mAdapter = new RecyclerBusAdapter(mCOntext, R.layout.bus_row, arrayListBuses);
                mRecyclerView.setAdapter(mAdapter);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    public void cargarRecyclerViewUniversidad(final RecyclerView mRecyclerView) {


        mDatabase.child("Buses").child("Universidad").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                ArrayList<BusModel> arrayListBuses = new ArrayList<>();

                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {

                    BusModel busModel = snapshot.getValue(BusModel.class);
                    busModel.setFecha((busModel.getFecha()));
                    busModel.setHora((busModel.getHora()));
                   // busModel.setCodigo((busModel.getCodigo()));
                    int num_asientos = busModel.getAsientos();
                    busModel.setAsientos(num_asientos);
                    busModel.setPlaca((busModel.getPlaca()));
                    busModel.setId_bus(busModel.getId_bus());

                    arrayListBuses.add(busModel);

                }

                mAdapter = new RecyclerBusAdapter(mCOntext, R.layout.bus_row, arrayListBuses);
                mRecyclerView.setAdapter(mAdapter);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    //este metodo lo ejecuta el dialog row de Agregar Buses
    public void cargaBusesFirebase(String fecha, String hora, int asientos, String placa, final Dialog dialog, final ProgressDialog progressDialog) {

        Map<String, Object> busCasa = new HashMap<>();
        busCasa.put("fecha", fecha);
        busCasa.put("hora", hora);
      //  busCasa.put("codigo", codigo);
        busCasa.put("asientos", asientos);
        busCasa.put("placa", placa);
        final DatabaseReference clave = mDatabase.child("Buses").child("Montecristi").push();
        final String id = clave.getKey();
        busCasa.put("id_bus", id);

        clave.updateChildren(busCasa).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                dialog.dismiss();
                progressDialog.dismiss();
                Toast.makeText(mCOntext, "Datos cargados correctamente", Toast.LENGTH_SHORT).show();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                progressDialog.dismiss();
                Toast.makeText(mCOntext, "Error al subir los datos" + e.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

        /*
        clave.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    final String asiento = dataSnapshot.child("asientos").getValue().toString();
                    int num_dato = Integer.parseInt(asiento);
                    DatabaseReference vecla = clave.child("lista_de_asientos");


                    for (int i = 1; i <= num_dato; i++) {
                        Map<String, Object> mapAsientos = new HashMap<>();
                        mapAsientos.put("Asientos: " + i, true);
                        vecla.updateChildren(mapAsientos);

                    }

                }

            }


            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        */


    }

    public void cargaBusesFirebaseUniversidad(String fecha, String hora, final int asientos, String placa, final Dialog dialog, final ProgressDialog progressDialog) {

        Map<String, Object> busUni = new HashMap<>();
        busUni.put("fecha", fecha);
        busUni.put("hora", hora);
     //   busUni.put("codigo", codigo);
        busUni.put("asientos", asientos);
        busUni.put("placa", placa);
        final DatabaseReference clave = mDatabase.child("Buses").child("Universidad").push();
        final String id = clave.getKey();
        busUni.put("id_bus", id);


        clave.updateChildren(busUni).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                dialog.dismiss();
                progressDialog.dismiss();
                Toast.makeText(mCOntext, "Datos cargados correctamente", Toast.LENGTH_SHORT).show();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                progressDialog.dismiss();
                Toast.makeText(mCOntext, "Error al subir los datos" + e.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

        /*clave.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    final String asiento = dataSnapshot.child("asientos").getValue().toString();
                    int num_dato = Integer.parseInt(asiento);
                    DatabaseReference vecla = clave.child("lista_de_asientos");


                    for (int i = 1; i <= num_dato; i++) {
                        Map<String, Object> mapAsientos = new HashMap<>();
                        mapAsientos.put("Asientos: " + i, true);
                        vecla.updateChildren(mapAsientos);

                    }

                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        */


    }

    public void Eliminar(String clave) {
        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
        mDatabase.child("Buses").child("Montecristi").child(clave).removeValue();

        DatabaseReference mDatabase2 = FirebaseDatabase.getInstance().getReference();
        mDatabase2.child("Buses").child("Universidad").child(clave).removeValue();


    }



    public void cargarListaUsuario(final RecyclerView mRecyclerView) {


        // para cargar los items se obtiene los datos de la firebase
        mDatabase.child("Usuarios").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                ArrayList<UserModel> arrayListUser = new ArrayList<>(); // creamoa ArrayList

                for (DataSnapshot snapshot : dataSnapshot.getChildren()) { //capturamos de los nodos hijos (es decir todas esa claves)
                    //el nombre de los datos de firebase deben ser los mismo que los de la clase BusModel
                    UserModel userModel = snapshot.getValue(UserModel.class);
                    userModel.setNombre((userModel.getNombre()));
                    userModel.setTelefono((userModel.getTelefono()));
                    userModel.setFacultad((userModel.getFacultad()));

                    arrayListUser.add(userModel);

                }


                //utilizamos la clase RecyclerBusAdapter
                mAdapterUser2 = new RecyclerUserAdapter2(mCOntext, R.layout.user_row_2, arrayListUser);
                mRecyclerView.setAdapter(mAdapterUser2);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }






    public void cargarReservaciones(final RecyclerView mRecyclerView,String destino,String destino2, String key_bus) {




        // para cargar los items se obtiene los datos de la firebase
        mDatabase.child("Buses").child(destino).child(key_bus).child("Reservaciones").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {


                if (dataSnapshot.exists()){
                    ArrayList<UserModel> arrayListUser = new ArrayList<>(); // creamoa ArrayList

                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) { //capturamos de los nodos hijos (es decir todas esa claves)
                        //el nombre de los datos de firebase deben ser los mismo que los de la clase BusModel
                        UserModel userModel = snapshot.getValue(UserModel.class);
                        userModel.setNombre((userModel.getNombre()));
                        userModel.setTelefono((userModel.getTelefono()));
                        userModel.setFacultad((userModel.getFacultad()));
                        int num_asiento =userModel.getAsiento();
                        userModel.setAsiento(num_asiento);

                        arrayListUser.add(userModel);

                    }


                    //utilizamos la clase RecyclerUserAdapter
                    mAdapterUser = new RecyclerUserAdapter(mCOntext, R.layout.user_row, arrayListUser);
                    mRecyclerView.setAdapter(mAdapterUser);


                }


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        mDatabase.child("Buses").child(destino2).child(key_bus).child("Reservaciones").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    ArrayList<UserModel> arrayListUser = new ArrayList<>(); // creamoa ArrayList

                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) { //capturamos de los nodos hijos (es decir todas esa claves)
                        //el nombre de los datos de firebase deben ser los mismo que los de la clase BusModel
                        UserModel userModel = snapshot.getValue(UserModel.class);
                        userModel.setNombre((userModel.getNombre()));
                        userModel.setTelefono((userModel.getTelefono()));
                        userModel.setFacultad((userModel.getFacultad()));
                        int num_asiento =userModel.getAsiento();
                        userModel.setAsiento(num_asiento);

                        arrayListUser.add(userModel);

                    }


                    //utilizamos la clase RecyclerUserAdapter
                    mAdapterUser = new RecyclerUserAdapter(mCOntext, R.layout.user_row, arrayListUser);
                    mRecyclerView.setAdapter(mAdapterUser);


                }


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }



}
