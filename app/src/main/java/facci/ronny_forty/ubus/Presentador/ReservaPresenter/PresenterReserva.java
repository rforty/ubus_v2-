package facci.ronny_forty.ubus.Presentador.ReservaPresenter;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;

import facci.ronny_forty.ubus.Modelo.UserModel;
import facci.ronny_forty.ubus.Vista.ReservaView.Reservaciones;

public class PresenterReserva {

    DatabaseReference mDatabase;
    Context mContext;
    FirebaseAuth mAuth;
    Button btnReservar;

    public PresenterReserva(DatabaseReference mDatabase, Context mContext, FirebaseAuth mAuth, Button btnReservar) {
        this.mDatabase = mDatabase;
        this.mContext = mContext;
        this.mAuth = mAuth;
        this.btnReservar = btnReservar;
    }


    public void DesactivarBtn() {
        btnReservar.setEnabled(false);
    }


    public void MostrarDatos(final String id_bus, final String destino, final TextView et1, final TextView et2, final TextView et3) {

        mDatabase.child("Buses").child(destino).child(id_bus).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    String fecha = dataSnapshot.child("fecha").getValue().toString();
                    String hora_salida = dataSnapshot.child("hora").getValue().toString();
                    String asiento = dataSnapshot.child("asientos").getValue().toString();
                    et1.setText(hora_salida);
                    et2.setText(asiento);
                    et3.setText(fecha);

                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }


    public void ConfirmacionReserva(final String destino, final String id_bus) {

        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:


                        Reservar(destino, id_bus);
                        DesactivarBtn();

                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle("Confirmar Reservación")
                .setPositiveButton("Reservar", dialogClickListener)
                .setNegativeButton("Cancelar", dialogClickListener)
                .show();

    }


    private void Reservar(final String destino, final String id_bus) {

        mDatabase.child("Buses").child(destino).child(id_bus).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {

                    final String asiento = dataSnapshot.child("asientos").getValue().toString();
                    int num_dato = Integer.parseInt(asiento);

                    int resta = num_dato - 1;
                    String fecha = dataSnapshot.child("fecha").getValue().toString();
                    String placa = dataSnapshot.child("placa").getValue().toString();
                    String hora_salida = dataSnapshot.child("hora").getValue().toString();
                    //   String codigo = dataSnapshot.child("codigo").getValue().toString();

                    String txt_key = id_bus;

                    mAuth = FirebaseAuth.getInstance();
                    DatabaseReference reservar = mDatabase.child("Usuarios").child(mAuth.getCurrentUser().getUid());

                    DatabaseReference reservar2 = mDatabase.child("Buses").child(destino).child(id_bus);






                    if (num_dato != 0) {

                        CrearReservacion(reservar, fecha, destino, hora_salida, placa, txt_key,num_dato);
                        CrearReservacionBUS(reservar2,num_dato);

                        Map<String, Object> busUni = new HashMap<>();
                        busUni.put("asientos", resta);

                        DatabaseReference drefe = mDatabase.child("Buses").child(destino).child(id_bus);
                        drefe.updateChildren(busUni);

                    } else {
                        Toast.makeText(mContext, "Asientos no disponibles", Toast.LENGTH_SHORT).show();
                    }


                } else {
                    Toast.makeText(mContext, "Este horario ya fue eliminado", Toast.LENGTH_SHORT).show();
                }

            }


            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }


    public void CrearReservacion(DatabaseReference claveReserva, String fecha, String destino, String hora, String placa, String key_bus, int asiento) {


        Map<String, Object> Reservado = new HashMap<>();

        Reservado.put("fecha", fecha);
        Reservado.put("destino", destino);
        Reservado.put("hora", hora);
        Reservado.put("asiento", asiento);
        Reservado.put("placa", placa);
        // Reservado.put("codigo", cod);
        Reservado.put("bus_key", key_bus);
        DatabaseReference CrearNodo = claveReserva.child("Reservaciones").push();
        final String id = CrearNodo.getKey();
        Reservado.put("clave", id);


        CrearNodo.updateChildren(Reservado).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                Toast.makeText(mContext, "Asiento Reservado ", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(mContext, Reservaciones.class);
                mContext.startActivity(intent);

                ((Activity) mContext).finish();

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                //progressDialog.dismiss();
                Toast.makeText(mContext, "Error al reservar asiento" + e.getMessage(), Toast.LENGTH_LONG).show();
            }
        });


    }

    public void CrearReservacionBUS (final DatabaseReference dr1, final int asiento){

        mDatabase.child("Usuarios").child(mAuth.getCurrentUser().getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String nombre = String.valueOf(dataSnapshot.child("nombre").getValue());
                String phone =String.valueOf(dataSnapshot.child("telefono").getValue());
                String facu =String.valueOf(dataSnapshot.child("facultad").getValue());

                Map<String, Object> Reservado = new HashMap<>();

                Reservado.put("nombre", nombre);
                Reservado.put("telefono", phone);
                Reservado.put("facultad", facu);
                Reservado.put("asiento",asiento);

               dr1.child("Reservaciones").push().updateChildren(Reservado);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }


}
