package facci.ronny_forty.ubus.Presentador.RegisterPresenter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.database.DatabaseReference;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import facci.ronny_forty.ubus.Vista.MenuView.MainActivity;

public class PresentadorRegistro {

    private Context mContext;
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;
    private String TAG = "PresentadorRegistro";


    public PresentadorRegistro(Context mContext, FirebaseAuth mAuth, DatabaseReference mDatabase) {
        this.mContext = mContext;
        this.mAuth = mAuth;
        this.mDatabase = mDatabase;
    }

    public void signUpUser(final String email, final String password, final String nombre, final String phone, final String facultad) {
        final ProgressDialog dialog = new ProgressDialog(mContext);
        dialog.setMessage("Registrando Usuario...");
        dialog.setCancelable(false);
        dialog.show();
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "createUserWithEmail:success");
                            dialog.dismiss();
                            Map<String, Object> crearUsuario = new HashMap<>();
                            crearUsuario.put("nombre", nombre);
                            crearUsuario.put("email", email);
                            // crearUsuario.put("contraseña", password);
                            crearUsuario.put("telefono", phone);
                            crearUsuario.put("facultad", facultad);
                            mDatabase.child("Usuarios").child(task.getResult().getUser().getUid()).updateChildren(crearUsuario);

                            Intent intent = new Intent(mContext, MainActivity.class);
                            mContext.startActivity(intent);

                        } else {

                            if (task.getException() instanceof FirebaseAuthUserCollisionException) { //si se presenta una colision
                                dialog.dismiss();
                                Toast.makeText(mContext, "Este correo ya esta registrado \n Intente con otro", Toast.LENGTH_LONG).show();
                            } else {
                                dialog.dismiss();
                                Log.w(TAG, "createUserWithEmail:failure", task.getException());
                                Toast.makeText(mContext, "Registro fallido", Toast.LENGTH_SHORT).show();

                            }

                        }
                    }
                });

    }

    public void ValidarInformacion(String email, String password, String nombre, String phone, String facultad) {

        // Patrón para validar el email
        Pattern patronEmail = Pattern.compile("^[a-z0-9-_\\+]+(\\.[a-z0-9-_]+)*@"
                + "[^ubus.com-]+[a-z0-9-_]+(\\.[a-z0-9_]+)*(\\.[a-z_]{2,})$");
        Matcher matherEmail = patronEmail.matcher(email);

        //Validad nombre de Usuario
        Pattern patronName = Pattern.compile("[^A-Za-z0-9áéíóú ]");
        Matcher matherName = patronName.matcher(nombre);

        //Condiciones aniladas para validad los campos
        if (TextUtils.isEmpty(nombre) || TextUtils.isEmpty(email) || TextUtils.isEmpty(password) || TextUtils.isEmpty(phone) || TextUtils.isEmpty(facultad)) {
            Toast.makeText(mContext, "Faltar llenar un campo", Toast.LENGTH_SHORT).show();
        } else if (matherName.find() == true) {
            Toast.makeText(mContext, "El nombre de usuario no puede tener simbolos", Toast.LENGTH_LONG).show();
        } else if (matherEmail.find() == false) {
            Toast.makeText(mContext, "Correo invalido", Toast.LENGTH_SHORT).show();
        } else if (password.length() < 6) {
            Toast.makeText(mContext, "La contraseña debe tener mas de 6 digitos", Toast.LENGTH_SHORT).show();
        } else if (phone.length() != 10) {
            Toast.makeText(mContext, "Debe ingresar un numero valido de 10 digitos", Toast.LENGTH_LONG).show();
        } else if (phone.startsWith("09")) {
            //se crea el registro
            signUpUser(email, password, nombre, phone, facultad);
        } else {
            Toast.makeText(mContext, "El numero debe empezar con 09", Toast.LENGTH_LONG).show();
        }

    }


}
