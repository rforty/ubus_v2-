package facci.ronny_forty.ubus.Presentador.ReservaPresenter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import facci.ronny_forty.ubus.Adaptadores.RecyclerReservaAdapter;
import facci.ronny_forty.ubus.Modelo.ReservacionModel;
import facci.ronny_forty.ubus.R;

public class PresenterReservaciones {

    private Context mContext;
    private DatabaseReference mDatabase;
    private FirebaseAuth mAuth;

    private RecyclerReservaAdapter mAdapter;

    public PresenterReservaciones(Context mContext, DatabaseReference mDatabase, FirebaseAuth mAuth) {
        this.mContext = mContext;
        this.mDatabase = mDatabase;
        this.mAuth = mAuth;
    }

    public void cargarRecyclerViewReservaciones(final RecyclerView mRecyclerView) {


        // para cargar los items se obtiene los datos de la firebase
        mDatabase.child("Usuarios").child(mAuth.getCurrentUser().getUid()).child("Reservaciones").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                ArrayList<ReservacionModel> arrayListReservas = new ArrayList<>(); // creamoa ArrayList

                for (DataSnapshot snapshot : dataSnapshot.getChildren()) { //capturamos de los nodos hijos (es decir todas esa claves)
                    //el nombre de los datos de firebase deben ser los mismo que los de la clase BusModel
                    ReservacionModel reservacionModel = snapshot.getValue(ReservacionModel.class);

                    reservacionModel.setDestino((reservacionModel.getDestino()));
                    reservacionModel.setFecha((reservacionModel.getFecha()));
                    reservacionModel.setHora((reservacionModel.getHora()));
                    int num_asiento =reservacionModel.getAsiento();
                    reservacionModel.setAsiento(num_asiento);
                    reservacionModel.setClave(reservacionModel.getClave());

                    arrayListReservas.add(reservacionModel);

                }


                //utilizamos la clase RecyclerBusAdapter
                mAdapter = new RecyclerReservaAdapter(mContext, R.layout.reserva_row, arrayListReservas);
                mRecyclerView.setAdapter(mAdapter);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

                Toast.makeText(mContext, "Fallo inesperado",Toast.LENGTH_LONG).show();

            }
        });

    }
}
