package facci.ronny_forty.ubus.Presentador.MainPresenter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

public class PresentadorMain {


    public void  SaludarUsuario (DatabaseReference mDatabase, FirebaseAuth mAuth, final Context mContext){

        mDatabase.child("Usuarios").child(mAuth.getCurrentUser().getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Toast.makeText(mContext,"Hola "+dataSnapshot.child("nombre").getValue(),Toast.LENGTH_SHORT).show();
                //et1.setText("Bienvenido "+dataSnapshot.child("nombre").getValue());
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }
}
