package facci.ronny_forty.ubus.Presentador.ReservadoPresenter;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;

public class PresenterReservado {

    FirebaseAuth mAuth;
    DatabaseReference mDatabaseRefe;
    Context mContext;
    String key;


    public PresenterReservado(FirebaseAuth mAuth, DatabaseReference mDatabaseRefe, Context mContext, String key) {
        this.mAuth = mAuth;
        this.mDatabaseRefe = mDatabaseRefe;
        this.mContext = mContext;
        this.key = key;
    }

    public void SendNameUser(final TextView tv_user) {
        mDatabaseRefe.child("Usuarios").child(mAuth.getCurrentUser().getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String usuario = String.valueOf(dataSnapshot.child("nombre").getValue());
                tv_user.setText(usuario);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    public void MostrarDatos(final TextView tv1, final TextView tv2, final TextView tv3, final TextView tv4,final TextView tv5) {


        mDatabaseRefe.child("Usuarios").child(mAuth.getCurrentUser().getUid()).child("Reservaciones").child(key)

                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists()) {

                            final String fecha = dataSnapshot.child("fecha").getValue().toString();
                            final String bus_key = dataSnapshot.child("bus_key").getValue().toString();
                            final String clave = dataSnapshot.child("clave").getValue().toString();
                            final String asiento = dataSnapshot.child("asiento").getValue().toString();
                         //   final String codigo = dataSnapshot.child("codigo").getValue().toString();
                            final String destino = dataSnapshot.child("destino").getValue().toString();
                            final String hora = dataSnapshot.child("hora").getValue().toString();
                            final String placa = dataSnapshot.child("placa").getValue().toString();

                            tv1.setText(hora);
                            tv2.setText(placa);
                           // tv3.setText(codigo);
                            tv3.setText(destino);
                            tv4.setText(fecha);
                            tv5.setText(asiento);

                        }

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Toast.makeText(mContext, "No hay conexión a Internet", Toast.LENGTH_LONG).show();

                    }
                });

    }



    public void EliminacionRegistro() {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:

                        mDatabaseRefe.child("Usuarios").child(mAuth.getCurrentUser().getUid()).child("Reservaciones").child(key).removeValue();
                        Toast.makeText(mContext, "Registro eliminado", Toast.LENGTH_SHORT).show();
                        //Intent i = new Intent(mContext, Reservaciones.class);
                        //mContext.startActivity(i);
                        ((Activity) mContext).finish();
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle("Eliminar Registro")
                .setMessage("Se recomienda solamente eliminar registros antiguos.")
                .setPositiveButton("   Si   ", dialogClickListener)
                .setNegativeButton("   No   ", dialogClickListener)
                .show();

    }



   /* public void CancelarReservacion() {
        mDatabaseRefe.child("Usuarios").child(mAuth.getCurrentUser().getUid()).child("Reservaciones").child(key)

                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists()) {


                            final String bus_key = dataSnapshot.child("bus_key").getValue().toString();
                            final String destino = dataSnapshot.child("destino").getValue().toString();

                            ConfirmarCancelacion(destino, bus_key);


                        }

                    }


                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Toast.makeText(mContext, "No hay conexión a Internet", Toast.LENGTH_LONG).show();

                    }
                });
    }
    */



/*
    public void ConfirmarCancelacion(final String desti, final String bus_key) {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        DevolverAsiento(desti,bus_key);

                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle("")

                .setPositiveButton("Cancelar Reservacion", dialogClickListener)
                .setNegativeButton("Retroceder", dialogClickListener)
                .show();

    }
    */


    /*
    public void DevolverAsiento(String destino, final String bus_key) {

        final DatabaseReference devolver = mDatabaseRefe.child("Buses").child(destino).child(bus_key);


        devolver.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {

                    final String asiento = dataSnapshot.child("asientos").getValue().toString();
                    int num_dato = Integer.parseInt(asiento);
                    int suma = num_dato + 1;


                    Map<String, Object> busUni = new HashMap<>();
                    busUni.put("asientos", suma);


                    devolver.updateChildren(busUni).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {

                            //devolver.child("Resevaciones").child(key_reservado).removeValue();


                            mDatabaseRefe.child("Usuarios").child(mAuth.getCurrentUser().getUid()).child("Reservaciones").child(key).removeValue();


                            Toast.makeText(mContext, "Se ha cancelado la reservación", Toast.LENGTH_SHORT).show();
                           // Intent i = new Intent(mContext, Reservaciones.class);
                           // mContext.startActivity(i);
                            ((Activity) mContext).finish();

                        }
                    });


                } else {
                    Toast.makeText(mContext, "Este horario ya fue eliminado", Toast.LENGTH_SHORT).show();

                    Map<String, Object> bus = new HashMap<>();
                    bus.put("asientos", 1);
                    bus.put("codigo", codigo);
                    bus.put("hora", hora);
                    bus.put("id_bus", bus_key);
                    bus.put("placa", placa);

                    devolver.updateChildren(bus);

                    mDatabaseRefe.child("Usuarios").child(mAuth.getCurrentUser().getUid()).child("Reservaciones").child(id_reserva).removeValue();

                    Toast.makeText(Asiento_Reservado.this, "Se ha cancelado la reservación", Toast.LENGTH_SHORT).show();
                    Intent i = new Intent(Asiento_Reservado.this, Reservaciones.class);
                    startActivity(i);
                    finish();


                }


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

                Toast.makeText(mContext, "Error inesperado", Toast.LENGTH_SHORT).show();

            }
        });


    }
    */


    /*public void EliminarRegistroAutomatico(String destino, String bus_key){

        final DatabaseReference nodoBus = mDatabaseRefe.child("Buses").child(destino).child(bus_key);


        nodoBus.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {

                }


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

                Toast.makeText(Asiento_Reservado.this, "Error inesperado", Toast.LENGTH_SHORT).show();

            }
        });



    }
    */




}
